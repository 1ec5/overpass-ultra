import { Protocol } from "pmtiles";
import { maplibregl, MapLibre } from "@trailstash/maplibre-custom-element";

import { normalizeCSS } from "./lib/normalize.js";

import { OverpassUltra } from "./components/overpass-ultra.js";
import { OverpassIDE } from "./components/overpass-ide.js";
import { OverpassMap } from "./components/overpass-map.js";
import { CodeEditor } from "./components/code-editor.js";
import { NavBar } from "./components/nav-bar.js";
import { RunButton } from "./components/run-button.js";
import { DownloadButton } from "./components/download-button.js";
import { StylePicker } from "./components/style-picker.js";
import { ShareModal as ShareButton } from "./components/share-modal.js";
import { HelpModal } from "./components/help-modal.js";
import { FSButton } from "./components/fs-button.js";
import { FontAwesomeIcon } from "./components/fontawesome-icon.js";
import { ButtonModal } from "./components/button-modal.js";
import { MapPopup } from "./components/map-popup.js";

// style
document.adoptedStyleSheets.push(normalizeCSS);

// maplibre plugins
const protocol = new Protocol();
maplibregl.addProtocol("pmtiles", protocol.tile);

maplibregl.setRTLTextPlugin("./mapbox-gl-rtl-text@0.2.3.js", true);

// register element
customElements.define("overpass-ide", OverpassIDE);
customElements.define("overpass-map", OverpassMap);
customElements.define("code-editor", CodeEditor);
customElements.define("map-libre", MapLibre);
customElements.define("nav-bar", NavBar);
customElements.define("run-button", RunButton);
customElements.define("download-button", DownloadButton);
customElements.define("share-button", ShareButton);
customElements.define("help-modal", HelpModal);
customElements.define("style-picker", StylePicker);
customElements.define("fs-button", FSButton);
customElements.define("fa-icon", FontAwesomeIcon);
customElements.define("button-modal", ButtonModal);
customElements.define("map-popup", MapPopup);
customElements.define("overpass-ultra", OverpassUltra);

// reload on hash changes
addEventListener("hashchange", () => window.location.reload());
