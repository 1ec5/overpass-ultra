// Proof-of-concept of portin OpenTrailStash
// not all controls are implemented yet

export default {
  // mode
  mode: "map",
  modes: {
    map: {
      // map settings
      zoom: 1,
      center: [0, 23],
      style: "https://open.trailsta.sh/style.json",
      options: {
        attributionControl: {
          customAttribution:
            '<a href="https://maplibre.org/" target="_blank">MapLibre</a>',
        },
        hash: true,
      },
      controls: [
        { type: "NavigationControl" },
        {
          type: "GeolocateControl",
          options: {
            positionOptions: {
              enableHighAccuracy: true,
            },
            trackUserLocation: true,
          },
        },
        {
          type: "FontAwesomeLinkControl",
          options: [
            "https://gitlab.com/trailstash/openstyle#opentrailstash",
            "About",
            "info",
          ],
        },
        { type: "MapSwapControl" },
        {
          position: "top-left",
          type: "OverpassSearch",
          options: {
            font: "Open Sans Bold",
            categories: [
              {
                name: "BMX/MTB Parks",
                query: `[bbox:{{bbox}}];
            (
              nwr[sport][leisure][sport~"(^|;)(bmx|mtb)(;|$)"];
              nwr[sport][leisure][name][sport~"(^|;)cycling(;|$)"][name~bmx,i];
            );
            out center;`,
                icon: "./dist/icons/jumps.png",
              },
              {
                name: "Skate Parks",
                query: `[bbox:{{bbox}}];nwr[sport~"(^|;)skateboard(;|$)"][!shop];out center;`,
                icon: "./dist/icons/skate_park.png",
              },
              {
                name: "Bike Repair Stations",
                query: `[bbox:{{bbox}}];node[amenity=bicycle_repair_station];out geom;`,
                icon: "./dist/icons/bike_repair_station.png",
              },
              {
                name: "Bike Shares",
                query: `[bbox:{{bbox}}];nwr[amenity=bicycle_rental][!shop][bicycle_rental!=shop];out center;`,
                icon: "./dist/icons/bike_share.png",
              },
              {
                name: "Toilets",
                query: `[bbox:{{bbox}}];(nwr[toilets=yes];nwr[amenity=toilets];);out center;`,
                icon: "./dist/icons/toilets.png",
              },
              {
                name: "Drinking Water",
                query: `[bbox:{{bbox}}];nwr[amenity=drinking_water];out center;`,
                icon: "./dist/icons/drinking_water.png",
              },
              {
                name: "Bike Shops",
                query: `[bbox:{{bbox}}];nwr[shop=bicycle];out center;`,
                icon: "./dist/icons/bike_shop.png",
              },
              {
                name: "Outfitters",
                query: `[bbox:{{bbox}}];nwr[shop=outdoor];out center;`,
                icon: "./dist/icons/outfitter.png",
              },
              {
                name: "Convenience Stores",
                query: `[bbox:{{bbox}}];nwr[shop=convenience];out center;`,
                icon: "./dist/icons/convenience_store.png",
              },
              {
                name: "Cafés",
                query: `[bbox:{{bbox}}];nwr[amenity=cafe];out center;`,
                icon: "./dist/icons/cafe.png",
              },
              {
                name: "Restaurants",
                query: `[bbox:{{bbox}}];nwr[amenity=restaurant];out center;`,
                icon: "./dist/icons/restaurant.png",
              },
              {
                name: "Fast Food",
                query: `[bbox:{{bbox}}];nwr[amenity=fast_food];out center;`,
                icon: "./dist/icons/fast_food.png",
              },
            ],
          },
        },
        {
          type: "ScaleControl",
          options: {
            unit: "imperial",
          },
        },
      ],
    },
  },
};
