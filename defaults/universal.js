import { maplibregl } from "@trailstash/maplibre-custom-element";

const queryUrl = () => {
  const params = new URLSearchParams(window.location.hash.slice(1));
  params.delete("map");
  return new URL("#" + params.toString(), window.location).toString();
};

const server = "https://overpass-api.de/api";
const style = {
  version: 8,
  sources: {
    OpenStreetMap: {
      type: "raster",
      tiles: ["https://tile.openstreetmap.org/{z}/{x}/{y}.png"],
      attribution:
        '\u003Ca href="https://www.openstreetmap.org/copyright" target="_blank"\u003E© OpenStreetMap contributors\u003C/a\u003E',
      tileSize: 256,
      minzoom: 0,
      maxzoom: 24,
    },
  },
  id: "OpenStreetMap",
  layers: [
    {
      type: "raster",
      id: "OpenStreetMap",
      source: "OpenStreetMap",
      minzoom: 0,
      maxzoom: 24,
    },
  ],
};
const querySources = ["OverpassAPI"];
const popupTemplate = `
  <h2>
    {{ type }}
    <a href="https://openstreetmap.org/{{ type }}/{{ id }}" target="_blank">{{ id }}</a>
    <a href="https://openstreetmap.org/edit?{{ type }}={{ id }}" target="_blank">✏️</a>
  </h2>
  <h3>Tags</h3>
  {%- for tag in tags %}
    {%- if tag[0] contains "website" %}
      <code>{{ tag[0] }} = <a href="{{ tag[1] }}" target="_blank">{{ tag[1] }}</a></code>
    {%- elsif tag[0] contains "wikidata" %}
      <code>{{ tag[0] }} = <a href="https://wikidata.org/wiki/{{ tag[1] }}" target="_blank">{{ tag[1] }}</a></code>
    {%- elsif tag[0] contains "wikipedia" %}
      {% assign lang = tag[1] | split: ":" | first %}
      <code>{{ tag[0] }} = <a href="https://{{ lang }}.wikipedia.org/wiki/{{ tag[1] | replace_first: lang, "" | replace_first: ":", "" }}" target="_blank">{{ tag[1] }}</a></code>
    {%- else %}
      <code>{{ tag[0] }} = {{ tag[1] }}</code>
    {%- endif %}
    <br>
  {%- endfor %}
  {%- if meta %}
    <h3>Meta</h3>
    {%- for tag in meta %}
      {%- if tag[0] == "changeset" %}
        <code>{{ tag[0] }} = <a href="https://openstreetmap.org/changeset/{{ tag[1] }}" target="_blank">{{ tag[1] }}</a></code>
      {%- elsif tag[0] == "user" %}
        <code>{{ tag[0] }} = <a href="https://openstreetmap.org/user/{{ tag[1] }}" target="_blank">{{ tag[1] }}</a></code>
      {%- else %}
        <code>{{ tag[0] }} = {{ tag[1] }}</code>
      {%- endif %}
        <br>
    {%- endfor %}
  {%- endif %}
  {%- if coordinates %}
    <h3>Coordinates</h3>
    <a href="geo://{{coordinates[1]}},{{coordinates[0]}}">{{coordinates[1] | round: 6 }} / {{coordinates[0] | round: 6 }}</a> <small>(lat/lon)</small>
  {%- endif %}
`;

export default {
  // mode
  mode: "ide",
  modes: {
    ide: {
      // overpass-ultra settings
      query: `/*
This is an example Overpass query.
Try it out by pressing the Run button above!
*/
[bbox:{{bbox}}];
(
way[highway=path];
way[highway=footway];
way[highway=cycleway];
way[highway=steps];
);
out geom;`,
      server,
      // map settings
      zoom: 16,
      center: [-77.4515, 37.5287],
      style,
      querySources,
      popupTemplate,
      options: {
        attributionControl: {},
        maxBounds: [
          [-179.999999999, -85.051129],
          [179.999999999, 85.051129],
        ],
      },
      controls: [new maplibregl.LogoControl()],
    },
    map: {
      // overpass-ultra settings
      query: "",
      server,
      // map settings
      zoom: 1,
      center: [0, 23],
      style,
      querySources,
      popupTemplate,
      options: {
        attributionControl: {
          customAttribution: `Powered by <a href=".">Overpass Ultra</a>
            (<a href="${queryUrl()}">View Query</a>)`,
        },
      },
      controls: [
        new maplibregl.LogoControl(),
        new maplibregl.FullscreenControl(),
      ],
    },
  },
};
