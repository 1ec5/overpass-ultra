import universal from "./universal.js";
import openTrailStash from "./opentrailstash.js";
import overpassUltra from "./overpass-ultra-us.js";
import ohmOverpassUltra from "./ohm-overpass-ultra-us.js";

let defaultOptions = universal;
if (window.location.host === "overpass-ultra.us") {
  defaultOptions = overpassUltra;
} else if (window.location.host === "beta.overpass-ultra.us") {
  // All standard overpass-ultra.us styles work on localhost:* & 127.0.0.1:* except for OMT styles
  defaultOptions = overpassUltra;
  defaultOptions.modes.ide.styles = defaultOptions.modes.ide.styles.filter(
    (x) => x[1].includes("protomaps") || x[1].includes("open.trailsta.sh"),
  );
  defaultOptions.modes.ide.style = defaultOptions.modes.ide.styles[0][1];
  defaultOptions.modes.map.style = defaultOptions.modes.ide.styles[0][1];
} else if (window.location.host === "localhost:8000") {
  // All standard overpass-ultra.us styles work on localhost:8080
  defaultOptions = overpassUltra;
} else if (
  window.location.host.endsWith(".github.io") ||
  window.location.host.startsWith("localhost:") ||
  window.location.host.startsWith("127.0.0.1:")
) {
  // All standard overpass-ultra.us styles work on localhost:* & 127.0.0.1:* & *.github.io except for protomaps
  defaultOptions = overpassUltra;
  defaultOptions.modes.ide.styles = defaultOptions.modes.ide.styles.filter(
    (x) => !x[1].includes("protomaps"),
  );
} else if (window.location.host === "ohm.overpass-ultra.us") {
  defaultOptions = ohmOverpassUltra;
}
Object.freeze(defaultOptions);
export default defaultOptions;
