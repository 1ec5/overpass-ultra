# Overpass Ultra

This is a webapp for running [Overpass API](http://www.overpass-api.de/), very much inspired by
[overpass turbo](https://github.com/tyrasd/overpass-turbo).

Because it uses MapLibre GL JS instead of Leaflet, it can be used to visualize significantly larger
Overpass API results than overpass turbo and can be styled using the [MapLibre style
specification](https://maplibre.org/maplibre-style-spec/) instead of MapCSS.

See the OSM Wiki for more documentation: https://wiki.openstreetmap.org/wiki/Overpass_Ultra

![screenshot](./screenshot.png)
