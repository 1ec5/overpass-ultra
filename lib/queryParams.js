import {
  compressToEncodedURIComponent,
  decompressFromEncodedURIComponent,
} from "lz-string";
import { setFrontmatterOptions } from "./frontmatter.js";

export const getModeFromQueryParams = () => {
  const params = new URLSearchParams(window.location.hash.slice(1));
  let mode;
  if (params.has("map")) {
    mode = "map";
  }
  return mode;
};

const fetchGist = async (query) => {
  let [gistID, filename] = query.slice("gist:".length).split("/", 2);
  const resp = await fetch(`https://api.github.com/gists/${gistID}`);
  if (!resp.ok) {
    throw new Error(
      `Failed to fetch ${resp.url}: ${resp.status} ${await resp.text()}`,
    );
  }
  const gist = await resp.json();
  if (!filename) {
    for (filename in gist.files) break;
  }
  return gist.files[filename].content;
};
const fetchURL = async (url) => {
  const resp = await fetch(url);
  if (!resp.ok) {
    throw new Error(
      `Failed to fetch ${resp.url}: ${resp.status} ${await resp.text()}`,
    );
  }
  return await resp.text();
};

export const settingsFromQueryParams = () => {
  const params = new URLSearchParams(window.location.hash.slice(1));
  const settings = {};

  // mapview params
  if (params.has("m")) {
    const m = params.get("m");
    if (m.includes(",")) {
      settings.fitBounds = m.split(",").map(parseFloat);
    } else {
      const [zoom, lat, lng] = m.split("/").map(parseFloat);
      settings.zoom = zoom;
      settings.center = [lng, lat];
    }
  }
  // autorun
  if (params.has("run")) {
    settings.autoRun = true;
  }

  // query
  if (params.has("q")) {
    settings.query = decompressFromEncodedURIComponent(params.get("q"));
  } else if (params.has("query")) {
    settings.query = params.get("query");
    if (settings.query.startsWith("gist:")) {
      settings.query = fetchGist(settings.query);
    } else if (settings.query.startsWith("url:")) {
      const url = settings.query.slice("url:".length);
      settings.query = fetchURL(url);
    }
  }

  // override style & server if provided
  // fetched configs can't be overwritten via style&server params
  if (!settings.query?.then) {
    if (params.has("style")) {
      settings.query = setFrontmatterOptions(settings.query || "", {
        style: params.get("style"),
      });
    }
    if (params.has("server")) {
      settings.query = setFrontmatterOptions(settings.query || "", {
        server: params.get("server"),
      });
    }
  }

  return settings;
};

export const toQueryParams = ({
  query,
  center,
  zoom,
  autoRun = false,
  mode,
}) => {
  const root = window.location.origin + window.location.pathname;
  const q = compressToEncodedURIComponent(query);
  let hash = "#";
  if (autoRun) {
    hash += "run&";
  }
  if (mode === "map") {
    hash += "map&";
  }
  if (center && zoom) {
    const zyx = [
      zoom.toFixed(2),
      center[1].toFixed(4),
      center[0].toFixed(4),
    ].join("/");
    hash += `m=${zyx}&`;
  }
  hash += `q=${q}`;
  return `${root}${hash}`;
};
