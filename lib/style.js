import { validateStyleMin, v8 } from "@maplibre/maplibre-gl-style-spec";

const overpassSource = {
  type: "geojson",
  data: { type: "FeatureCollection", features: [] },
  generateId: true,
};
const overpassLayers = [
  {
    id: "overpass-polygons",
    type: "fill",
    source: "OverpassAPI",
    filter: ["all", ["==", ["geometry-type"], "Polygon"]],
    paint: {
      "fill-color": "rgba(255, 204, 0, .5)",
    },
  },
  {
    id: "overpass-polygons-stroke",
    type: "line",
    source: "OverpassAPI",
    filter: ["all", ["==", ["geometry-type"], "Polygon"]],
    paint: { "line-width": 2, "line-color": "rgba(0, 51, 255, 0.6)" },
  },
  {
    id: "overpass-lines",
    type: "line",
    source: "OverpassAPI",
    filter: ["all", ["==", ["geometry-type"], "LineString"]],
    paint: {
      "line-width": 5,
      "line-color": "rgba(0, 51, 255, 0.6)",
    },
    layout: { "line-cap": "round" },
  },
  {
    id: "overpass-poi",
    type: "circle",
    source: "OverpassAPI",
    filter: ["all", ["==", ["geometry-type"], "Point"]],
    paint: {
      "circle-stroke-width": 2,
      "circle-stroke-color": "rgba(0, 51, 255, 0.6)",
      "circle-color": "rgba(255, 204, 0, 0.6)",
    },
  },
];

export function setBaseStyle(style, baseStyle, overwrite = false) {
  if (typeof style === "string") {
    return overwrite ? baseStyle : style;
  } else if (overwrite) {
    return {
      ...style,
      extends: baseStyle,
    };
  } else {
    return {
      extends: baseStyle,
      ...style,
    };
  }
}

export async function getStyle(style) {
  if (typeof style === "string") {
    const resp = await fetch(style);
    const styleJSON = await resp.json();
    return ensureSourceAndLayers(styleJSON);
  } else if (style && !style.version) {
    let parentStyle;
    if (typeof style.extends === "string") {
      const resp = await fetch(style.extends);
      parentStyle = await resp.json();
    } else {
      parentStyle = style.extends;
    }
    const layers = [...parentStyle.layers];
    for (const i in style.layers) {
      const { beforeLayerId, ...layer } = style.layers[i];
      let layerIdx = -1;
      if (beforeLayerId) {
        layerIdx = layers.findIndex(({ id }) => id === beforeLayerId);
      }
      if (layerIdx >= 0) {
        layers.splice(layerIdx, 0, ensureLayerProps(layer, i));
      } else {
        layers.push(ensureLayerProps(layer, i));
      }
    }
    const extendedStyle = {
      ...parentStyle,
      ...style,
      sources: { ...parentStyle.sources, ...(style.sources || {}) },
      layers,
    };
    return ensureSourceAndLayers(extendedStyle);
  } else {
    return ensureSourceAndLayers({
      ...style,
      layers: style.layers?.map(ensureLayerProps),
    });
  }
}

function ensureLayerProps(layer, i) {
  if (!layer.id) {
    layer.id = `overpass-ultra-${i}`;
  }
  if (!layer.source) {
    layer.source = "OverpassAPI";
  }
  return layer;
}

export function ensureSourceAndLayers(style) {
  if (!style.sources) {
    style.sources = {};
  }
  if (!style.sources.OverpassAPI) {
    style.sources.OverpassAPI = { ...overpassSource };
    let foundAttribution = false;
    for (const source in style.sources) {
      if (
        style.sources[source].attribution
          ?.toLowerCase()
          .includes("openstreetmap") ||
        style.sources[source].url?.startsWith("https://tile.ourmap.us")
      ) {
        foundAttribution = true;
      }
    }
    if (!foundAttribution) {
      style.sources.OverpassAPI.attribution =
        '\u003Ca href="https://www.openstreetmap.org/copyright" target="_blank"\u003E© OpenStreetMap contributors\u003C/a\u003E';
    }
  }
  if (!style.layers) {
    style.layers = [];
  }
  let hasOverpassLayer = false;
  for (const { source } of style.layers) {
    if (source === "OverpassAPI") {
      hasOverpassLayer = true;
      break;
    }
  }
  if (!hasOverpassLayer) {
    for (const layer of overpassLayers) {
      style.layers.push(layer);
    }
  }
  const errors = validateStyleMin(style, v8);
  if (errors.length) {
    throw new Error(
      "Invalid style:\n\n" + errors.map(({ message }) => message).join("\n"),
    );
  }
  return style;
}

export const handleStyleImageMissing = (ev) => {
  if (ev.id.startsWith("https://") || ev.id.startsWith("data:")) {
    ev.target.loadImage(ev.id, (error, image) => {
      if (error) throw error;
      if (!ev.target.hasImage(ev.id)) {
        ev.target.addImage(ev.id, image);
      }
    });
  }
};
