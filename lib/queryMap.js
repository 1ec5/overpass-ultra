import { maplibregl } from "@trailstash/maplibre-custom-element";
import { h } from "../lib/dom.js";

export const handleMouseMove = (e, popupTemplate, querySources) => {
  if (!popupTemplate) {
    return;
  }
  const features = e.target
    .queryRenderedFeatures(e.point)
    .filter((l) => querySources?.includes(l.source));
  if (features.length > 0) {
    e.target.getCanvas().style.cursor = "pointer";
  } else {
    e.target.getCanvas().style.cursor = "";
  }
};

export const handleMouseClick = (e, popupTemplate, querySources) => {
  if (!popupTemplate) {
    return;
  }
  const features = e.target
    .queryRenderedFeatures(e.point)
    .filter((l) => querySources?.includes(l.source));
  const distinctFeatures = {};
  for (const f of features) {
    distinctFeatures[f.id] = f;
  }
  const popup = h("map-popup");
  popup.features = distinctFeatures;
  if (popupTemplate) {
    popup.template = popupTemplate;
  }
  if (features.length > 0) {
    new maplibregl.Popup()
      .setLngLat(e.lngLat)
      .setDOMContent(popup)
      .addTo(e.target);
  }
};
