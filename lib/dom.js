// DOM generation helpers

export const h = (elName, attrs, ...children) => {
  const el = document.createElement(elName);
  for (const key in attrs) {
    if (key in el) {
      el[key] = attrs[key];
    } else {
      el.setAttribute(key, attrs[key]);
    }
  }
  if (children.length === 1 && typeof children[0] === "string") {
    el.innerHTML = children[0];
  } else {
    for (const child of children) {
      el.appendChild(child);
    }
  }
  return el;
};
export const t = (text) => document.createTextNode(text);
