export const localStorage = {
  getItem: (key) => {
    try {
      return JSON.parse(window.localStorage.getItem(key));
    } catch {
      return null;
    }
  },
  setItem: (key, value) => {
    try {
      return window.localStorage.setItem(key, JSON.stringify(value));
    } catch {
      return;
    }
  },
};

export default localStorage;

export const settingsFromStorage = () => {
  const settings = {};
  const query = localStorage.getItem("query");
  const center = localStorage.getItem("mapCenter");
  const zoom = localStorage.getItem("mapZoom");
  if (query) {
    settings.query = query;
  }
  if (center) {
    settings.center = center;
  }
  if (zoom) {
    settings.zoom = zoom;
  }
  return settings;
};
