const fetchedImages = {};
export const handleStyleImageMissing = async (ev) => {
  const builtin = ev.id.match(/\/icons\/(maki|temaki)\//);
  if (ev.id.startsWith("https://") || ev.id.startsWith("data:") || builtin) {
    if (!fetchedImages[ev.id]) {
      fetchedImages[ev.id] = true;
      const image = await ev.target.loadImage(ev.id);
      ev.target.addImage(ev.id, image.data, { sdf: builtin });
    }
  }
};
