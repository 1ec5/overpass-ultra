import normalize from "normalize.css/normalize.css";
import base from "./base.css";

export const normalizeCSS = new CSSStyleSheet();
normalizeCSS.replaceSync(normalize + base);
