import osmtogeojson from "osmtogeojson";

export async function callInterpreter(
  query,
  bbox,
  controller,
  apiRoot = "https://overpass-api.de/api",
) {
  const body = query.replace(
    /{{bbox}}/g,
    `${bbox.getSouth()},${bbox.getWest()},${bbox.getNorth()},${bbox.getEast()}`,
  );
  const resp = await fetch(apiRoot + "/interpreter", {
    body,
    method: "POST",
    mode: "cors",
    signal: controller.signal,
  });
  if (
    resp.status == 200 &&
    !resp.headers.get("content-type").startsWith("text/html")
  ) {
    let geoJSON;
    if (resp.headers.get("content-type") == "application/osm3s+xml") {
      geoJSON = osmtogeojson(
        new window.DOMParser().parseFromString(await resp.text(), "text/xml"),
        { verbose: true, flatProperties: false },
      );
    } else {
      geoJSON = osmtogeojson(await resp.json(), {
        flatProperties: false,
      });
    }
    geoJSON.features = geoJSON.features.map((x) => {
      const properties = x.properties;
      delete x.properties;
      return {
        properties: {
          "@id": properties.id,
          "@type": properties.type,
          "@meta":
            Object.keys(properties.meta).length > 0
              ? properties.meta
              : undefined,
          ...properties.tags,
        },
        ...x,
      };
    });
    return geoJSON;
  } else {
    const dom = new window.DOMParser().parseFromString(
      await resp.text(),
      "text/xml",
    );
    alert(
      Array.from(dom.body.querySelectorAll("p"))
        .slice(1)
        .map((p) => p.textContent)
        .join("\n"),
    );
    return { type: "FeatureCollection", features: [] };
  }
}
