import YAML from "yaml";

const extractFrontMatter = (content) => {
  if (!content) {
    return { query: "", frontmatter: "" };
  }
  const result = /(^---\n((?<frontmatter>.*)\n)?---(\n|$))?(?<query>.*)?/s.exec(
    content,
  );
  if (result) {
    return {
      query: result.groups.query?.trim() || "",
      frontmatter: result.groups.frontmatter || "",
    };
  }
  return { query: content?.trim() };
};

export const settingsFromFrontmatter = (query) => {
  let frontmatter;
  ({ frontmatter, query } = extractFrontMatter(query));
  let settings = {};
  if (frontmatter) {
    settings = YAML.parse(frontmatter, { merge: true }) || {};
    const doc = YAML.parseDocument(frontmatter, undefined, {
      merge: true,
    });
    if (doc.warnings.length > 0) {
      alert(
        "Warnings when parsing frontmatter:\n\n" +
          doc.warnings.map(({ message }) => message).join("\n"),
      );
    }
  }
  settings.query = query;
  return settings;
};

export const setFrontmatterOptions = (query, { style, server }) => {
  let frontmatter;
  ({ frontmatter, query } = extractFrontMatter(query));
  let settingsStr = "";
  if (style) {
    settingsStr += `style: ${style}\n`;
  }
  if (server) {
    settingsStr += `server: ${server}\n`;
  }
  if (frontmatter && frontmatter.trim()) {
    const settings = YAML.parseDocument(frontmatter, undefined, {
      merge: true,
    });
    if (style) {
      const queryStyle = settings.get("style");
      if (!queryStyle || typeof queryStyle === "string") {
        settings.set("style", style);
      } else {
        queryStyle.set("extends", style);
      }
    }
    if (server) {
      settings.set("server", server);
    }
    settingsStr = YAML.stringify(settings);
  }

  return `---\n${settingsStr}---\n${query}`;
};
