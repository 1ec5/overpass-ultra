#!/bin/bash
set -e
set -x

# https://github.com/bazelbuild/rules_closure/issues/351#issuecomment-854628326
export OPENSSL_CONF=/dev/null

mkdir -p static/icons/{maki,temaki}

curl -L -o maki.zip https://github.com/mapbox/maki/archive/refs/heads/main.zip
unzip -u maki.zip
pushd static/icons/maki
cp ../../../maki-main/LICENSE* .
for icon in $(ls ../../../maki-main/icons/*); do
    name=$(basename $icon)
    name=${name%.*}
    mkdir $name
    npx -- svg2png $icon -o $name/icon.png -w 30 -h 30
    npx -- sprite-one ./$name -i $name/ --sdf
    rm -rf $name $name.json
done
popd
rm -rf maki.zip maki-main

curl -L -o temaki.zip https://github.com/rapideditor/temaki/archive/refs/heads/main.zip
unzip -u temaki.zip
pushd static/icons/temaki
cp ../../../temaki-main/LICENSE* .
for icon in $(ls ../../../temaki-main/icons/*); do
    name=$(basename $icon)
    name=${name%.*}
    mkdir $name
    npx -- svg2png $icon -o $name/icon.png -w 30 -h 30
    npx -- sprite-one ./$name -i $name/ --sdf
    rm -rf $name $name.json
done
popd
rm -rf temaki.zip temaki-main
