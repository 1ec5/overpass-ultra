import { icon } from "@fortawesome/fontawesome-svg-core";
import {
  faUpRightAndDownLeftFromCenter as faMaximize,
  faDownLeftAndUpRightToCenter as faMinimize,
} from "@fortawesome/free-solid-svg-icons";

const faMaximizeIcon = icon(faMaximize);
const faMinimizeIcon = icon(faMinimize);

import { h } from "../lib/dom.js";
import { normalizeCSS } from "../lib/normalize.js";
import { style as buttonCSS } from "./button.js";

const style = new CSSStyleSheet();
style.replaceSync(`
  :host {
  }
  button {
    background: white;
  }
`);

export class FSButton extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    shadow.adoptedStyleSheets.push(normalizeCSS);
    shadow.adoptedStyleSheets.push(buttonCSS);
    shadow.adoptedStyleSheets.push(style);

    const icon = h("fa-icon", { icon: "up-right-and-down-left-from-center" });
    const button = h("button", {}, icon);
    this.refs = { button, icon };

    button.addEventListener("click", (e) => {
      if (this.classList.contains("fs")) {
        this.refs.icon.icon = "up-right-and-down-left-from-center";
        this.dispatchEvent(new Event("exit-fullscreen"));
      } else {
        this.refs.icon.icon = "down-left-and-up-right-to-center";
        this.dispatchEvent(new Event("enter-fullscreen"));
      }
      this.classList.toggle("fs");
    });

    shadow.appendChild(button);
  }
}
