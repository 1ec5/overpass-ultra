import { h } from "../lib/dom.js";
import { getModeFromQueryParams } from "../lib/queryParams.js";

import defaults from "../defaults/auto.js";

const style = new CSSStyleSheet();
style.replaceSync(`
  :host, main {
    height: 100%;
    width: 100%;
    display: flex;
  }
`);

export class OverpassUltra extends HTMLElement {
  constructor() {
    super();
  }

  async connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });
    shadow.adoptedStyleSheets.push(style);
    const mode = getModeFromQueryParams() || defaults.mode;
    const modeDefaults = defaults.modes[mode];
    if (mode === "map") {
      shadow.appendChild(h("overpass-map", { options: modeDefaults }));
    } else {
      shadow.appendChild(h("overpass-ide", { options: modeDefaults }));
    }
  }
}
