import { maplibregl } from "@trailstash/maplibre-custom-element";
import { h } from "../lib/dom.js";
import { callInterpreter } from "../lib/overpass.js";
import { setBaseStyle, getStyle } from "../lib/style.js";
import {
  setFrontmatterOptions,
  settingsFromFrontmatter,
} from "../lib/frontmatter.js";
import { settingsFromQueryParams, toQueryParams } from "../lib/queryParams.js";
import { localStorage, settingsFromStorage } from "../lib/localStorage.js";
import { handleStyleImageMissing } from "../lib/sprites.js";
import { handleMouseClick, handleMouseMove } from "../lib/queryMap.js";

const style = new CSSStyleSheet();
style.replaceSync(`
  :host {
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
  }
  main {
    display: flex;
    flex-direction: row;
    flex-grow: 1;
  }
  code-editor {
    width: 45%;
    border: none;
    border-right: 1px solid #8f8f9d;
    resize: horizontal;
    overflow: auto;
  }
  map-libre {
    flex-grow: 1;
    height: 100%;
  }
  @media (max-width: 900px) {
    main {
      flex-direction: column;
    }
    code-editor {
      width: auto;
      height: 45%;
      resize: vertical;
    }
  }
  code-editor.fs {
    display: none;
  }
  .fs style-picker,
  .fs share-button,
  .fs help-modal,
  .fs download-button {
    display: none;
  }
  .fs run-button {
    z-index: 1;
    display: block;
    position: fixed;
    top: 4px;
    left: 4px;
  }
  fs-button {
    z-index: 1;
    position: fixed;
    top: 4px;
    right: 4px;
  }
  :host(:fullscreen) code-editor, :host(:fullscreen) nav-bar {
    display: none;
  }
  run-button, download-button, share-button, style-picker, help-modal {
    margin-right: 4px;
  }
`);

export class OverpassIDE extends HTMLElement {
  constructor() {
    super();
    // initialize options
    this.options = {};
    // Bind methods
    this.onClickRun = this.onClickRun.bind(this);
    this.onClickAbort = this.onClickAbort.bind(this);
    this.onAuxClickRun = this.onAuxClickRun.bind(this);
    this.onChangeCode = this.onChangeCode.bind(this);
    this.onMoveEnd = this.onMoveEnd.bind(this);
    this.onClickShare = this.onClickShare.bind(this);
    this.onChangeStyle = this.onChangeStyle.bind(this);
    this.onEnterFullscreen = this.onEnterFullscreen.bind(this);
    this.onExitFullscreen = this.onExitFullscreen.bind(this);
  }

  connectedCallback() {
    // Create & populate Shadow DOM
    const shadow = this.attachShadow({ mode: "open" });
    shadow.adoptedStyleSheets.push(style);
    shadow.appendChild(
      h(
        "nav-bar",
        {},
        h("fs-button", { slot: "fs-button" }),
        h(
          "span",
          { slot: "buttons" },
          h("run-button"),
          h("download-button"),
          h("share-button"),
          h("style-picker", { styles: this.options.styles }),
          h("help-modal"),
        ),
      ),
    );

    // Get initial editor(query) and map(cener&zoom) state
    Object.assign(this.options, settingsFromStorage());
    Object.assign(this.options, settingsFromQueryParams());
    this.popupTemplate = this.options.popupTemplate;
    this.querySources = this.options.querySources;

    shadow.appendChild(
      h(
        "main",
        {},
        h("code-editor", {
          autofocus: true,
          source: this.options.query.then ? "" : this.options.query,
        }),
        h("map-libre", {
          options: this.options.options,
          controls: this.options.controls,
          style: this.options.style,
          zoom: this.options.zoom,
          center: this.options.center,
        }),
      ),
    );

    this.refs = {
      navBar: shadow.querySelector("nav-bar"),
      codeEditor: shadow.querySelector("code-editor"),
      mapLibre: shadow.querySelector("map-libre"),
      runButton: shadow.querySelector("run-button"),
      downloadButton: shadow.querySelector("download-button"),
      shareButton: shadow.querySelector("share-button"),
      stylePicker: shadow.querySelector("style-picker"),
      fsButton: shadow.querySelector("fs-button"),
    };

    // Setup Event Listeners
    this.refs.codeEditor.addEventListener("change", this.onChangeCode);
    this.refs.runButton.addEventListener("run", this.onClickRun);
    this.refs.codeEditor.addEventListener("run", this.onClickRun);
    this.refs.runButton.addEventListener("auxclick", this.onAuxClickRun);
    this.refs.runButton.addEventListener("abort", this.onClickAbort);
    this.refs.shareButton.addEventListener("click", this.onClickShare);
    this.refs.stylePicker.addEventListener("change", this.onChangeStyle);
    this.refs.fsButton.addEventListener(
      "enter-fullscreen",
      this.onEnterFullscreen,
    );
    this.refs.fsButton.addEventListener(
      "exit-fullscreen",
      this.onExitFullscreen,
    );
    this.refs.mapLibre.map.on("moveend", this.onMoveEnd);
    this.refs.mapLibre.map.on("click", (e) =>
      handleMouseClick(e, this.popupTemplate, this.querySources),
    );
    this.refs.mapLibre.map.on("mousemove", (e) =>
      handleMouseMove(e, this.popupTemplate, this.querySources),
    );
    this.refs.mapLibre.map.on("styleimagemissing", handleStyleImageMissing);

    // set query when resolved if a promise
    if (this.options.query.then) {
      this.options.query.then((query) => {
        this.refs.codeEditor.source = query;
        if (this.options.autoRun) {
          this.onClickRun();
        }
      });
    } else if (this.options.autoRun) {
      this.onClickRun();
    }
  }

  async onClickRun() {
    this.refs.runButton.loading = true;
    this.refs.downloadButton.data = null;

    try {
      const options = { ...this.options };
      Object.assign(options, settingsFromFrontmatter(options.query));
      options.style = setBaseStyle(options.style, this.options.style);

      this.refs.mapLibre.style = await getStyle(options.style);
      this.popupTemplate = options.popupTemplate;
      this.querySources = options.querySources;

      if (!options.query) {
        this.refs.runButton.loading = false;
        return;
      }
      this.controller = new AbortController();
      const data = await callInterpreter(
        options.query,
        this.refs.mapLibre.bounds,
        this.controller,
        options.server,
      );
      delete this.controller;

      this.refs.downloadButton.data = data;
      this.refs.mapLibre.map
        .getSource("OverpassAPI")
        ?.setData(data)
        .once("data", () => {
          this.refs.runButton.loading = false;
        });
    } catch (err) {
      this.refs.downloadButton.data = null;
      this.refs.mapLibre.map
        .getSource("OverpassAPI")
        ?.setData({ type: "FeatureCollection", features: [] });
      this.refs.runButton.loading = false;
      if (err.name != "AbortError") {
        alert(err);
        throw err;
      }
    }
  }
  onClickAbort() {
    if (this.controller) {
      this.controller.abort();
      delete this.controller;
    }
    this.refs.runButton.loading = false;
  }
  async onAuxClickRun() {
    window.location = toQueryParams({ ...this.options, mode: "map" });
  }
  onChangeCode() {
    this.options.query = this.refs.codeEditor.source;
    localStorage.setItem("query", this.options.query);
  }
  onMoveEnd() {
    this.options.zoom = this.refs.mapLibre.zoom;
    this.options.center = this.refs.mapLibre.center.toArray();
    localStorage.setItem("mapZoom", this.options.zoom);
    localStorage.setItem("mapCenter", this.options.center);
  }
  onClickShare() {
    this.refs.shareButton.center = this.options.center;
    this.refs.shareButton.zoom = this.options.zoom;
    this.refs.shareButton.query = this.options.query;
  }
  onChangeStyle(e) {
    this.refs.codeEditor.source = setFrontmatterOptions(this.options.query, {
      style: e.detail.value,
    });
  }

  onEnterFullscreen() {
    this.refs.navBar.classList.add("fs");
    this.refs.codeEditor.classList.add("fs");
  }

  onExitFullscreen() {
    this.refs.navBar.classList.remove("fs");
    this.refs.codeEditor.classList.remove("fs");
  }
}
