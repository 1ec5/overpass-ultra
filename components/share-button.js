import { h } from "../lib/dom.js";
import buttonStyle from "./button.css";
import { style as buttonCSS } from "./button.js";
import { normalizeCSS } from "../lib/normalize.js";

export const css = new CSSStyleSheet();
css.replaceSync(`
  button:after {
    content: " Share";
  }
  @media (max-width: 900px) {
    button:after {
      content: "";
    }
  }
`);

export class ShareButton extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    shadow.adoptedStyleSheets.push(normalizeCSS);
    shadow.adoptedStyleSheets.push(buttonCSS);
    shadow.adoptedStyleSheets.push(css);

    const button = h("button", {}, h("fa-icon", { icon: "link" }));
    this.refs = { button };

    shadow.appendChild(button);
  }
}
