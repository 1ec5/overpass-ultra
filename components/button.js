import { h } from "../lib/dom.js";
import buttonStyle from "./button.css";
import { normalizeCSS } from "../lib/normalize.js";

export const style = new CSSStyleSheet();
style.replaceSync(buttonStyle);
