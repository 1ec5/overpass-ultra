import { icon, library } from "@fortawesome/fontawesome-svg-core";
import {
  faLink,
  faShareNodes,
  faQuestion,
  faDownload,
  faPlay,
  faXmark,
  faUpRightAndDownLeftFromCenter,
  faDownLeftAndUpRightToCenter,
  faPaintbrush,
  faPenToSquare,
} from "@fortawesome/free-solid-svg-icons";

import { h } from "../lib/dom.js";
import buttonStyle from "./button.css";
import { style as buttonCSS } from "./button.js";
import { normalizeCSS } from "../lib/normalize.js";

library.add(faLink);
library.add(faShareNodes);
library.add(faQuestion);
library.add(faDownload);
library.add(faPlay);
library.add(faXmark);
library.add(faUpRightAndDownLeftFromCenter);
library.add(faDownLeftAndUpRightToCenter);
library.add(faPaintbrush);
library.add(faPenToSquare);

export const css = new CSSStyleSheet();
css.replaceSync(`
  :host {
    display: inline-block;
    vertical-align: top;
  }
  svg {
    display: inline-block;
    height: 1em;
    width: 1em;
    vertical-align: top;
    padding: 1px 0;
  }
`);

export class FontAwesomeIcon extends HTMLElement {
  static observedAttributes = ["icon"];

  get icon() {
    return this.getAttribute("icon");
  }
  set icon(value) {
    this.setAttribute("icon", value);
  }

  constructor() {
    super();
  }

  connectedCallback() {
    this.shadow = this.attachShadow({ mode: "open" });

    this.shadow.adoptedStyleSheets.push(normalizeCSS);
    this.shadow.adoptedStyleSheets.push(css);

    const { html } = icon({ prefix: "fas", iconName: this.icon });

    this.shadow.innerHTML = html[0];
  }

  attributeChangedCallback(name, oldVal, newVal) {
    if (!this.shadow) {
      return;
    }

    const { html } = icon({ prefix: "fas", iconName: this.icon });

    this.shadow.innerHTML = html[0];
  }
}
