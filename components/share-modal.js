import { h, t } from "../lib/dom.js";
import { style as buttonCSS } from "./button.js";
import { normalizeCSS } from "../lib/normalize.js";
import { toQueryParams } from "../lib/queryParams.js";

export class ShareModal extends HTMLElement {
  #center;
  #zoom;
  #query;

  constructor() {
    super();
  }

  get center() {
    return this.#center;
  }
  set center(value) {
    this.#center = value;
    this.update();
  }

  get zoom() {
    return this.#zoom;
  }
  set zoom(value) {
    this.#zoom = value;
    this.update();
  }

  get query() {
    return this.#query;
  }
  set query(value) {
    this.#query = value;
    this.update();
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    const queryInput = h("input", { type: "text" });
    const mapInput = h("input", { type: "text" });
    const mapViewCheckBox = h("input", { type: "checkbox", checked: true });
    mapViewCheckBox.addEventListener("click", (e) => {
      e.stopPropagation();
      this.update();
    });
    const mapViewLabel = h(
      "label",
      {},
      mapViewCheckBox,
      t(" include current map state"),
    );
    mapViewLabel.addEventListener("click", (e) => {
      e.stopPropagation();
    });
    const autoRunCheckBox = h("input", { type: "checkbox" });
    autoRunCheckBox.addEventListener("click", (e) => {
      e.stopPropagation();
      this.update();
    });
    const autoRunLabel = h(
      "label",
      {},
      autoRunCheckBox,
      t(" run this query immediately after loading"),
    );
    autoRunLabel.addEventListener("click", (e) => {
      e.stopPropagation();
    });
    const div = h(
      "div",
      { style: "", slot: "modal-content" },
      h(
        "style",
        {},
        `
        div {
          padding: 0 8px 8px;
        }
        h3, h5 {
          margin-bottom: 0;
        }
        input[type=text] {
          width: 100%;
          padding: 0;
          margin: 0;
        }
        label {
          display: block;
        }
        `,
      ),
      h("h3", {}, "Query"),
      h("p", {}, "Copy this link to share the current code:"),
      queryInput,
      h("h5", {}, "Options"),
      mapViewLabel,
      autoRunLabel,
      h("h3", {}, "Map"),
      h("p", {}, "Copy this link to share the an interactive map:"),
      mapInput,
    );
    const button = h(
      "button-modal",
      { text: "Share", icon: "share-nodes" },
      div,
    );
    this.refs = {
      button,
      div,
      mapInput,
      queryInput,
      autoRunCheckBox,
      mapViewCheckBox,
    };

    queryInput.addEventListener("focus", () => queryInput.select());
    mapInput.addEventListener("focus", () => mapInput.select());

    shadow.appendChild(button);
  }

  update() {
    const opts = { query: this.query, mode: "ide" };
    if (this.refs.mapViewCheckBox.checked) {
      opts.center = this.center;
      opts.zoom = this.zoom;
    }
    opts.autoRun = this.refs.autoRunCheckBox.checked;
    this.refs.queryInput.value = toQueryParams(opts);
    delete opts.autoRun;
    opts.center = this.center;
    opts.zoom = this.zoom;
    opts.mode = "map";
    this.refs.mapInput.value = toQueryParams(opts);
  }
}
