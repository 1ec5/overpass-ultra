import { h } from "../lib/dom.js";
import { normalizeCSS } from "../lib/normalize.js";
import { style as buttonCSS } from "./button.js";

const style = new CSSStyleSheet();
style.replaceSync(`
  :host {
  }
  button {
    background: #2ecc40;
    width: 100px;
    color: white;
  }
  button:after {
    content: " Run";
  }
  button.loading {
    background: #ff4136;
  }
  button.loading:after {
    content: " Cancel";
  }
`);

export class RunButton extends HTMLElement {
  static observedAttributes = ["loading"];

  get loading() {
    return this.hasAttribute("loading");
  }
  set loading(value) {
    if (value) {
      return this.setAttribute("loading", "");
    } else {
      return this.removeAttribute("loading");
    }
  }

  constructor() {
    super();
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    shadow.adoptedStyleSheets.push(normalizeCSS);
    shadow.adoptedStyleSheets.push(buttonCSS);
    shadow.adoptedStyleSheets.push(style);

    const button = h("button", {}, h("fa-icon", { icon: "play" }));
    this.refs = { button };

    button.addEventListener("auxclick", (e) => {
      this.dispatchEvent(new Event("auxclick"));
    });
    button.addEventListener("click", (e) => {
      if (this.loading) {
        this.refs.button.children[0].icon = "play";
        this.dispatchEvent(new Event("abort"));
      } else {
        this.refs.button.children[0].icon = "xmark";
        this.dispatchEvent(new Event("run"));
      }
    });

    shadow.appendChild(button);
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (newValue === "") {
      this.refs.button.classList.add("loading");
      this.refs.button.children[0].icon = "xmark";
    } else {
      this.refs.button.classList.remove("loading");
      this.refs.button.children[0].icon = "play";
    }
  }
}
