import { h, t } from "../lib/dom.js";
import logo from "../static/logo.svg";

const style = new CSSStyleSheet();
style.replaceSync(`
  :host {
    display: block;
  }
  nav {
    background: white;
    border-bottom: 1px solid #8f8f9d;
    padding: 4px;
  }
  :host(.fs) nav {
    background: none;
    border: none;
    padding: 0;
  }
  a {
    color: black;
    text-decoration: none;
  }
  b, img {
    margin: 0 0 0 10px;
  }
  img {
    vertical-align: middle;
    height: 22px;
  }
  nav > * {
    margin-right: 4px;
  }
  slot {
    padding: 100px;
  }
  ::slotted(fs-button) {
    z-index: 1;
    position: fixed;
    top: 4px;
    right: 4px;
  }
  :host(.fs) nav > a {
    display: none;
  }
  @media (max-width: 900px) {
    b {
      display: none;
    }
  }
`);

export class NavBar extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });
    shadow.adoptedStyleSheets.push(style);
    const nav = h(
      "nav",
      {},
      h("slot", { name: "fs-button" }),
      h("slot", { name: "buttons" }),
      h(
        "a",
        { href: "." },
        h("b", {}, t("Overpass Ultra")),
        h("img", { src: logo }),
      ),
    );
    shadow.appendChild(nav);
  }
}
