import { h, t } from "../lib/dom.js";
import { style as buttonCSS } from "./button.js";
import { normalizeCSS } from "../lib/normalize.js";
import { toQueryParams } from "../lib/queryParams.js";
import { maplibreVersion } from "@trailstash/maplibre-custom-element";
import { version as osmtogeojsonVersion } from "osmtogeojson/package.json";

export class HelpModal extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    const div = h(
      "div",
      { style: "padding: 0 8px 8px;max-width:800px;", slot: "modal-content" },
      `
          <h4>Introduction</h4>
          <p>
            <i>Overpass Ultra</i> is a reimagining of
            <a href="https://overpass-turbo.eu/">overpass turbo</a>
            powered by
            <a href="https://maplibre.org/projects/maplibre-gl-js/"
              >MapLibre GL JS</a
            >. Like overpass turbo, it is a web-based data filtering &amp;
            visualization tool for <a href="https://osm.org">OpenStreetMap</a>.
          </p>
          <p>
            Thanks to MapLibre GL JS's excellent large GeoJSON support, Overpass
            Ultra can visualize larger datasets than overpass turbo with ease.
          </p>
          <p>
            More information about how to write
            <a
              href="https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL"
              >Overpass queries</a
            >
            can be found in the OSM wiki.
          </p>

          <h4>Overpass Queries</h4>
          <p>
            Overpass API allows to query for OSM data by your own search
            criteria. For this purpose, it has a specifically crafted
            <a
              href="https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL"
              >query language</a
            >.
          </p>
          <p>
            In addition to regular Overpass API queries one can use
            <code>{{bbox}}</code> to specify the bounding box coordinates of the
            current map view.
          </p>

          <h4>Configuration</h4> Various aspects of <i>Overpass Ultra</i>, such as styling and the
          Overpass API server to use, can be configured via
          <a href="https://wiki.openstreetmap.org/wiki/Overpass_Ultra/Extensions#YAML_front-matter" target="_blank">YAML frontmatter</a>.
          <h5>Styling</h5>
          <p>
            In-place of overpass turbo's MapCSS support, <i>Overpass Ultra</i>
            supports styling via the <code>style:</code> key in the YAML frontmatter. It can contain:
            <ul>
              <li>An URL to a <code>style.json</code></a>
              <li>An entire MapLibre style object</li>
              <li>A partial MapLibre style containing only a <code>layers:</code> array to extend
              an existing style's layers (You can omit the <code>souce</code> and <code>id</code>
              keys in the layer objects)</li>
            </ul>
            More information on styling results in <i>Overpass Ultra</i> on the
            <a href="https://wiki.openstreetmap.org/wiki/Overpass_Ultra/Styling" target="_blank">OSM Wiki</a>.
          </p>

          <h4>About</h4>
          <img
            alt="powered by Overpass API"
            style="float: right"
            src="https://wiki.openstreetmap.org/w/images/b/b3/Powered_by_Overpass_API.png"
          />
          <p>
            <i>Overpass Ultra</i> is built by Daniel Schep.
          </p>
          <p>
            You can contact me on <a rel="me" href="https://mapstodon.space/@trailstash">Mastodon</a>.
          </p>
          <h4>Feedback, Bug Reports, Feature Requests</h4>
          <p>
            Overpass Ultra is still in early stages of development and should be
            considered experimental.
          </p>
          <p>
            If you would like to report a bug or provide other feedback, please
            do so in the project's
            <a href="https://gitlab.com/trailstash/overpass-ultra/-/issues"
              >Issue Tracker</a
            >
          </p>
          <h4>Source Code</h4>
          <p>
            The
            <a href="https://gitlab.com/trailstash/overpass-ultra"
              >source code</a
            >
            of this application is released under the
            <a
              href="https://gitlab.com/trailstash/overpass-ultra/-/blob/master/LICENSE"
              >MIT license</a
            >.
          </p>

          <div>
            <h5>Data Sources</h5>
            <ul>
              <li>
                Data ©
                <a href="http://openstreetmap.org/">OpenStreetMap</a>
                contributors,
                <span style="font-size: smaller">
                  <a href="http://opendatacommons.org/licenses/odbl/1-0/"
                    >ODbL</a
                  >
                  (<a href="http://www.openstreetmap.org/copyright">Terms</a>)
                </span>
              </li>
              <li>
                Data mining by
                <a href="http://overpass-api.de/">Overpass API</a>
              </li>
              <li>
                <a href="https://www.openmaptiles.org/"
                  >OpenMapTiles
                </a>
                hosted by the
                <a href="http://tile.ourmap.us/">
                  OSM Americana Community Vector Tile Server</a
                >
              </li>
              <li>
                <a href="https://www.protomaps.com/"
                  >Protomaps
                </a>
                hosted by the Protomaps API
              </li>
            </ul>
            <h5>Software &amp; Libraries</h5>
            <ul>
              <li>
                Map powered by
                <a href="https://maplibre.org/projects/maplibre-gl-js/"
                  >MapLibre GL JS</a
                > <code>${maplibreVersion}</code>
              </li>
              <li>
                OSM to GeoJSON conversion powered by
                <a href="https://github.com/tyrasd/osmtogeojson"
                  >osmtogeojson</a
                > <code>${osmtogeojsonVersion}</code>
              </li>
            </ul>
          </div>
      `,
    );
    div.addEventListener("click", (e) => e.stopPropagation());
    const button = h("button-modal", { text: "Help", icon: "question" }, div);
    this.refs = { button, div };

    shadow.appendChild(button);
  }
}
