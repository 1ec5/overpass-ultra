import { h, t } from "../lib/dom.js";
import { style as buttonCSS } from "./button.js";
import { normalizeCSS } from "../lib/normalize.js";

export class ButtonModal extends HTMLElement {
  constructor() {
    super();
  }
  get text() {
    return this.getAttribute("text");
  }
  set text(value) {
    return this.setAttribute("text", value);
  }
  get icon() {
    return this.getAttribute("icon");
  }
  set icon(value) {
    return this.setAttribute("icon", value);
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    const css = new CSSStyleSheet();
    css.replaceSync(`
      button {
        position: relative;
      }
      button div span.close {
        cursor: pointer;
        float: right;
      }
      button.button-modal:after {
        content: " ${this.text}";
      }
      button > div {
        cursor: default;
        display: none;
        z-index: 1;
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0,0,0,0.5);
      }
      button > div.visible {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        padding: 20px;
      }
      button > div > div {
        max-height: 100%;
        max-width: 100%;
        overflow: auto;
        cursor: default;
        background: white;
        border: 1px solid #8f8f9d;
        border-radius: 4px;
        text-align: left;
        padding: 4px 8px;
      }
      @media (max-width: 900px) {
        button.button-modal:after {
          content: "";
        }
      }
    `);

    shadow.adoptedStyleSheets.push(normalizeCSS);
    shadow.adoptedStyleSheets.push(buttonCSS);
    shadow.adoptedStyleSheets.push(css);

    const div = h(
      "div",
      {},
      h("span", {}, h("fa-icon", { icon: this.icon }), t(" " + this.text)),
      h("span", { class: "close" }, "×"),
      h("slot", { name: "modal-content" }),
    );
    const backdrop = h("div", {}, div);
    const button = h(
      "button",
      { class: "button-modal" },
      h("fa-icon", { icon: this.icon }),
      backdrop,
    );
    this.refs = { button, div, backdrop };

    button.addEventListener("click", () => {
      this.toggle();
    });
    div.addEventListener("click", (e) => {
      e.preventDefault();
      e.stopPropagation();
    });
    div.querySelector("span.close").addEventListener("click", (e) => {
      this.toggle();
      e.preventDefault();
      e.stopPropagation();
    });

    shadow.appendChild(button);
  }
  toggle() {
    this.refs.backdrop.classList.toggle("visible");
  }
}
