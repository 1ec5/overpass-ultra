import { Liquid } from "liquidjs";
import { h, t } from "../lib/dom.js";
import { normalizeCSS } from "../lib/normalize.js";

const engine = new Liquid();

const style = new CSSStyleSheet();
style.replaceSync(`
  a {
    text-decoration: none;
  }
  h1,h2,h3,h4,h5,h6 {
    margin: .5em 0;
  }
`);

const defaultTemplate = `
  <h2>
    {{ type }}
    <a href="https://openstreetmap.org/{{ type }}/{{ id }}" target="_blank">{{ id }}</a>
    <a href="https://openstreetmap.org/edit?{{ type }}={{ id }}" target="_blank">✏️</a>
  </h2>
  <h3>Tags</h3>
  {%- for tag in tags %}
    {%- if tag[0] contains "website" %}
      <code>{{ tag[0] }} = <a href="{{ tag[1] }}" target="_blank">{{ tag[1] }}</a></code>
    {%- elsif tag[0] contains "wikidata" %}
      <code>{{ tag[0] }} = <a href="https://wikidata.org/wiki/{{ tag[1] }}" target="_blank">{{ tag[1] }}</a></code>
    {%- elsif tag[0] contains "wikipedia" %}
      {% assign lang = tag[1] | split: ":" | first %}
      <code>{{ tag[0] }} = <a href="https://{{ lang }}.wikipedia.org/wiki/{{ tag[1] | replace_first: lang, "" | replace_first: ":", "" }}" target="_blank">{{ tag[1] }}</a></code>
    {%- else %}
      <code>{{ tag[0] }} = {{ tag[1] }}</code>
    {%- endif %}
    <br>
  {%- endfor %}
  {%- if meta %}
    <h3>Meta</h3>
    {%- for tag in meta %}
      {%- if tag[0] == "changeset" %}
        <code>{{ tag[0] }} = <a href="https://openstreetmap.org/changeset/{{ tag[1] }}" target="_blank">{{ tag[1] }}</a></code>
      {%- elsif tag[0] == "user" %}
        <code>{{ tag[0] }} = <a href="https://openstreetmap.org/user/{{ tag[1] }}" target="_blank">{{ tag[1] }}</a></code>
      {%- else %}
        <code>{{ tag[0] }} = {{ tag[1] }}</code>
      {%- endif %}
        <br>
    {%- endfor %}
  {%- endif %}
  {%- if coordinates %}
    <h3>Coordinates</h3>
    <a href="geo://{{coordinates[1]}},{{coordinates[0]}}">{{coordinates[1] | round: 6 }} / {{coordinates[0] | round: 6 }}</a> <small>(lat/lon)</small>
  {%- endif %}
`;

export class MapPopup extends HTMLElement {
  constructor() {
    super();
    this.template = defaultTemplate;
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    shadow.adoptedStyleSheets.push(normalizeCSS);
    shadow.adoptedStyleSheets.push(style);

    const div = h("div");
    let first = true;
    for (const f of Object.values(this.features)) {
      if (first) {
        first = false;
      } else {
        div.appendChild(h("hr"));
      }
      const templateContext = {
        id: f.properties["@id"],
        type: f.properties["@type"],
        meta: JSON.parse(f.properties["@meta"] || null),
        tags: { ...f.properties },
      };
      delete templateContext.tags["@type"];
      delete templateContext.tags["@id"];
      delete templateContext.tags["@meta"];
      if (f.properties["@type"] === "node") {
        templateContext.coordinates = f.geometry.coordinates;
      }
      const html = engine.parseAndRenderSync(this.template, templateContext);
      div.appendChild(h("div", {}, html));
    }
    shadow.appendChild(div);
  }
}
