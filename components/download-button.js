import { h } from "../lib/dom.js";
import { normalizeCSS } from "../lib/normalize.js";
import { style as buttonCSS } from "./button.js";

const style = new CSSStyleSheet();
style.replaceSync(`
  button:after {
    content: " Download";
  }
  @media (max-width: 900px) {
    button:after {
      content: "";
    }
  }
`);

export class DownloadButton extends HTMLElement {
  #data;

  constructor() {
    super();
  }
  set data(data) {
    this.#data = data;
    this.refs.button.disabled = !this.#data;
  }
  get data() {
    return this.#data;
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    shadow.adoptedStyleSheets.push(normalizeCSS);
    shadow.adoptedStyleSheets.push(buttonCSS);
    shadow.adoptedStyleSheets.push(style);

    const button = h("button", {}, h("fa-icon", { icon: "download" }));
    button.disabled = !this.#data;
    this.refs = { button };

    button.addEventListener("click", (e) => {
      const json = JSON.stringify(this.data);
      const blob = new Blob([json], { type: "octet/stream" });
      const url = window.URL.createObjectURL(blob);

      const link = h("a", { download: "overpass.geojson", href: url });

      // this is necessary as link.click() does not work on the latest firefox
      link.dispatchEvent(
        new MouseEvent("click", {
          bubbles: true,
          cancelable: true,
          view: window,
        }),
      );

      setTimeout(() => {
        // For Firefox it is necessary to delay revoking the ObjectURL
        window.URL.revokeObjectURL(url);
        link.remove();
      }, 100);
    });

    shadow.appendChild(button);
  }

  attributeChangedCallback(name, oldValue, newValue) {}
}
