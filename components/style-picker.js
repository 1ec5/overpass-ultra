import { h, t } from "../lib/dom.js";
import { style as buttonCSS } from "./button.js";
import { normalizeCSS } from "../lib/normalize.js";

const style = `margin: 10px 0; display: block; width: 100%; text-align: left;`;

export class StylePicker extends HTMLElement {
  constructor() {
    super();
    this.styles = [];
  }

  connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });

    if (!this.styles || this.styles.length === 0) {
      return;
    }

    const div = h("div", { slot: "modal-content" });
    const button = h(
      "button-modal",
      { text: "Pick Style", icon: "paintbrush" },
      div,
    );
    this.refs = { button, div };

    for (const [name, value, tiles] of this.styles) {
      const styleButton = h(
        "button",
        { style },
        t(name),
        // h("br"),
        // h("small", {}, `Tiles: ${tiles}`),
      );
      styleButton.addEventListener("click", (e) => {
        button.toggle();
        this.dispatchEvent(
          new CustomEvent("change", { detail: { value: value } }),
        );
      });
      div.appendChild(styleButton);
    }

    shadow.appendChild(button);
  }
}
