import { maplibregl } from "@trailstash/maplibre-custom-element";

import { h } from "../lib/dom.js";
import { callInterpreter } from "../lib/overpass.js";
import { setBaseStyle, getStyle } from "../lib/style.js";
import { settingsFromFrontmatter } from "../lib/frontmatter.js";
import { settingsFromQueryParams } from "../lib/queryParams.js";
import { handleStyleImageMissing } from "../lib/sprites.js";
import { handleMouseClick, handleMouseMove } from "../lib/queryMap.js";

const style = new CSSStyleSheet();
style.replaceSync(`
  :host, main {
    height: 100%;
    width: 100%;
    display: flex;
    flex-direction: column;
  }
`);

export class OverpassMap extends HTMLElement {
  constructor() {
    super();
    this.options = {};
  }

  async connectedCallback() {
    const shadow = this.attachShadow({ mode: "open" });
    shadow.adoptedStyleSheets.push(style);

    const options = { ...this.options, ...settingsFromQueryParams() };
    Object.assign(
      options,
      settingsFromFrontmatter(await Promise.resolve(options.query)),
    );
    options.style = setBaseStyle(options.style, this.options.style);
    this.popupTemplate = options.popupTemplate;
    this.querySources = options.querySources;

    this.refs = {
      mapLibre: h("map-libre", {
        options: options.options,
        center: options.center,
        zoom: options.zoom,
        controls: options.controls,
        style: await getStyle(options.style),
      }),
    };
    shadow.appendChild(h("main", {}, this.refs.mapLibre));

    // setup event listeners
    this.refs.mapLibre.map.on("click", (e) =>
      handleMouseClick(e, this.popupTemplate, this.querySources),
    );
    this.refs.mapLibre.map.on("mousemove", (e) =>
      handleMouseMove(e, this.popupTemplate, this.querySources),
    );
    this.refs.mapLibre.map.on("styleimagemissing", handleStyleImageMissing);

    if (!options.query) {
      return;
    }

    try {
      this.controller = new AbortController();
      const data = await callInterpreter(
        options.query,
        this.refs.mapLibre.bounds,
        this.controller,
        options.server,
      );
      this.refs.mapLibre.map.getSource("OverpassAPI").setData(data);
    } catch (err) {
      if (err.name != "AbortError") {
        alert(err);
        throw err;
      }
    }
  }
}
